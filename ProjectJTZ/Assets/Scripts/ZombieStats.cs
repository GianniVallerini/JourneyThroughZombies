﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieStats : MonoBehaviour {

    public SO_StatisticheZombie SO_statZ;
    public GameObject ammoPack;

    [HideInInspector]
    public int hp;

    private void Start()
    {
        hp = SO_statZ.hp;
    }

    void Update () {

        if(hp <= 0)
        {
            int rnd = Random.Range(0,100);
            GameObject spawned;

            if (rnd < SO_statZ.ammoDropPercent)
            {
                spawned = Instantiate(ammoPack, new Vector3(transform.position.x, 0.1f, transform.position.z), transform.rotation);
                spawned.transform.Rotate(Vector3.forward, Random.Range(0f,90f));
                spawned.transform.Rotate(Vector3.up, 90);
                spawned.transform.Rotate(Vector3.forward, 90);
            }
            Destroy(gameObject);
        }

	}
}
