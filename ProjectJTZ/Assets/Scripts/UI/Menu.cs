﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public void OnclickStart()
    {
        StartCoroutine(LoadScene());
        
        
    }
    public void OnclickQuit()
    {
        Application.Quit();
    }
    IEnumerator LoadScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MapEditor");
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }


}
