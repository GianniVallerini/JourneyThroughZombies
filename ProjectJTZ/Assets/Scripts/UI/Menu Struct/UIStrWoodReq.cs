﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStrWoodReq : MonoBehaviour {

    public BuildingPreview buildingScript;

    void Update () {

        gameObject.GetComponent<Text>().text = "" + buildingScript.selectedStruct.woodReq;

    }
}
