﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBstMetalReq : MonoBehaviour {

    public BoostSystem boostSystemScript;
            
    void Update () {

        gameObject.GetComponent<Text>().text = "" + boostSystemScript.selectedBoost.metalReq;

    }
}
