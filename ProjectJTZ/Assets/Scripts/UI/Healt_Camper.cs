﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healt_Camper : MonoBehaviour
{
    private float MaxHealth { get; set; }
    public SO_GameController gm;
    public SO_GameController gmValues;
    public Slider healthBar;
    public SO_ListaSprites SO_SpritesCamper;
    GameObject camperHull;

    private void Awake()
    {
        camperHull = GameObject.Find("CamperHull");
        
    }
    void Start()
    {
        camperHull.GetComponent<Animator>().enabled = false;
        MaxHealth = gmValues.camperHp;

        healthBar.value = CalculationHealt();
    }

    void Update()
    {
        healthBar.value = CalculationHealt();

        if (healthBar.value == 1)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[0];
        }
        if (healthBar.value <= 0.9f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[1];
        }
        if (healthBar.value <= 0.8f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[2];
        }
        if (healthBar.value <= 0.7f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[3];
        }
        if (healthBar.value <= 0.6f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[4];
        }
        if (healthBar.value <= 0.5f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[5];
        }
        if (healthBar.value <= 0.4f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[6];
        }
        if (healthBar.value <= 0.3f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[7];
        }
        if (healthBar.value <= 0.2f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[8];
        }
        if (healthBar.value <= 0.1f)
        {
            camperHull.GetComponent<SpriteRenderer>().sprite = SO_SpritesCamper.Lista[9];
        }
        if (healthBar.value <= 0)
        {
            camperHull.GetComponent<Animator>().enabled = true;
            camperHull.GetComponent<Animator>().SetBool("Explosion", true);
        }



    }

    float CalculationHealt()
    {

        return gm.camperHp / MaxHealth;
    }
}
