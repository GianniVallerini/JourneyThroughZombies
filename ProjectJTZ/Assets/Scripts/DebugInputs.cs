﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugInputs : MonoBehaviour {

    GameObject[] allSpawners;
    public SO_GameController gm;
    public SO_GameController gmValues;
    public SO_Input SO_input;
    bool spawnsStopped = false;

    private void Start()
    {
        allSpawners = GameObject.FindGameObjectsWithTag("ZombieSpawner");
    }

    void Update () {

        if (Input.GetKeyDown(SO_input.debugSpawnZombieWave))
        {
            foreach (GameObject go in allSpawners)
            {
                go.GetComponent<ZombieSpawner>().SpawnAll();
            }
        }

        if (Input.GetKeyDown(SO_input.debugGoToMenu))
        {
            SceneManager.LoadScene("Menu");
        }

        if (Input.GetKeyDown(SO_input.debugStopStartSpawns))
        {
            if (spawnsStopped == false)
            {
                foreach (GameObject go in allSpawners)
                {
                    go.GetComponent<ZombieSpawner>().enabled = false;
                }
                spawnsStopped = true;
            }
            else
            {
                foreach (GameObject go in allSpawners)
                {
                    go.GetComponent<ZombieSpawner>().enabled = true;
                }
                spawnsStopped = false;
            }
        }

        if (Input.GetKey(SO_input.debugHealthCheat))
        {
            gm.camperHp++;
            gm.playerHp++;
        }
    
        if (Input.GetKey(SO_input.debugAmmoCheat))
        {
            gm.ammo++;
        }

        if (Input.GetKeyDown(SO_input.debugResetGameController))
        {
            gm.ammo = gmValues.ammo;
            gm.camperHp = gmValues.camperHp;
            gm.playerHp = gmValues.playerHp;
            gm.erbs = gmValues.erbs;
            gm.meds = gmValues.meds;
            gm.metal = gmValues.metal;
            gm.wood = gmValues.wood;
			gm.mode = gmValues.mode;
			gm.playerMode = gmValues.playerMode;
        }
    }
}
