﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimento : MonoBehaviour {
    public SO_StatistichePlayer SO_statPlayer;
    public SO_Input SO_input;
    //public float mapHeight = 20f;
    //public float mapWidth = 20f;

    Rigidbody rb;
    MovimentoB moveSetB;
    float speed = 0;
    float maxStamina = 0;


    private void Start()
    {
        speed = SO_statPlayer.speed;
        maxStamina = SO_statPlayer.stamina;
        rb = GetComponent<Rigidbody>();
        moveSetB = GetComponent<MovimentoB>();
    }
    void Update()
    {
        if (rb.velocity != Vector3.zero) rb.velocity = Vector3.zero;

        if (Input.GetKey(SO_input.sprint))
        {
            if (SO_statPlayer.stamina > 0)
            {
                speed = SO_statPlayer.sprintSpeed;
                SO_statPlayer.stamina -= SO_statPlayer.riduzioneStamina;
            }
            else
                speed = SO_statPlayer.speed;
        }
        else
        {
            speed = SO_statPlayer.speed;
            if (SO_statPlayer.stamina < maxStamina)
                SO_statPlayer.stamina += SO_statPlayer.riduzioneStamina;
            else
                SO_statPlayer.stamina = maxStamina;
        }
    }
    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.W) /*&& transform.position.z < mapHeight/2*/)
        {
            transform.position += Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S) /*&& transform.position.z > -(mapHeight / 2)*/)
        {
            transform.position -= Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D) /*&& transform.position.x < mapWidth / 2*/)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A)/* && transform.position.x > -(mapWidth / 2)*/)
        {
            transform.position -= Vector3.right * speed * Time.deltaTime;
        }
    }
}
