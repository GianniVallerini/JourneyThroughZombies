﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewCollisions : MonoBehaviour {

    //[HideInInspector]
	public int collidingThings = 0;


    private void OnTriggerEnter(Collider other)
    {
        collidingThings++;
    }

    private void OnTriggerExit(Collider other)
    {
        collidingThings--;
    }
}
