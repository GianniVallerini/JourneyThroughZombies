﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BuildingPreview : MonoBehaviour {

    public SO_InventarioOggetti SO_structList;
    public SO_GameController SO_GM;
    public SO_Input SO_input;
    public float distFromPlayer;
    public float rotationSpd = 1;
    public Color placeableColor;
    public Color notPlaceableColor;

    [HideInInspector]
    public SO_Struttura selectedStruct;

    float rotation = 90;
    GameObject spawned = null;
    bool placeable = false;
    bool enoughRes = true;
    int index = 0;

    private void OnEnable()
    {
        NewStructure();
    }

    void Update () {

        if(enoughRes)
            CheckPlaceablePlace(); //migliorabile con un evento.Raise() nel PreviewCollisions!
        spawned.transform.position = transform.position + transform.right * distFromPlayer;
        spawned.transform.LookAt(transform);
        spawned.transform.Rotate(Vector3.right, 90);
        spawned.transform.Rotate(Vector3.forward, rotation);

        if (Input.GetKey(SO_input.ruotaBuildOrario))
            rotation += rotationSpd;
        if (Input.GetKey(SO_input.ruotaBuildAntiorario))
            rotation -= rotationSpd;

        //piazza la struttura
        if(Input.GetKeyDown(SO_input.piazzamento) && placeable)
        {
            PlaceStructure();
            SO_GM.wood -= SO_structList.ListaStruct[index].woodReq;
            SO_GM.metal -= SO_structList.ListaStruct[index].metalReq;
            SO_GM.erbs -= SO_structList.ListaStruct[index].erbsReq;
            NewStructure();
        }

        //scroll tra la lista di oggetti
        if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (index < SO_structList.ListaStruct.Count - 1)
                index++;
            else
                index = 0;

            DestroyStructure();
            NewStructure();
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (index > 0)
                index--;
            else
                index = SO_structList.ListaStruct.Count - 1;

            DestroyStructure();
            NewStructure();
        }
    }

    private void OnDisable()
    {
        DestroyStructure();
    }

    void CheckPlaceableResources()
    {
        if (SO_GM.wood >= SO_structList.ListaStruct[index].woodReq && SO_GM.metal >= SO_structList.ListaStruct[index].metalReq && SO_GM.erbs >= SO_structList.ListaStruct[index].erbsReq)
            enoughRes = true;
        else
        {
            enoughRes = false;
            placeable = false;
            spawned.GetComponent<SpriteRenderer>().color = notPlaceableColor;
        }
    }

    void CheckPlaceablePlace()
    {
        if (spawned.GetComponent<PreviewCollisions>().collidingThings > 0)
        {
            placeable = false;
            spawned.GetComponent<SpriteRenderer>().color = notPlaceableColor;
        }
        else
        {
            placeable = true;
            spawned.GetComponent<SpriteRenderer>().color = placeableColor;
        }
    }

    void NewStructure()
    {
        spawned = Instantiate(SO_structList.ListaStruct[index].prefab, transform.position + transform.right * distFromPlayer, transform.rotation);
        selectedStruct = SO_structList.ListaStruct[index];
        CheckPlaceableResources();
        spawned.GetComponent<Collider>().isTrigger = true;
        spawned.AddComponent<PreviewCollisions>();
        if(spawned.GetComponent<NavMeshObstacle>())
        {
            spawned.GetComponent<NavMeshObstacle>().enabled = false;
        }
    }

    void PlaceStructure()
    {
        spawned.GetComponent<Collider>().isTrigger = false;
        spawned.GetComponent<PreviewCollisions>().enabled = false;
        spawned.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1);
        if (spawned.GetComponent<NavMeshObstacle>())
        {
            spawned.GetComponent<NavMeshObstacle>().enabled = true;
        }
    }

    void DestroyStructure()
    {
        Destroy(spawned);
        spawned = null;
    }
}
