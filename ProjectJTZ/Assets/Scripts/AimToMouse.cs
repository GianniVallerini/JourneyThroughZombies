﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimToMouse : MonoBehaviour {
    Animator tor;
    Animator pla;
    public SO_GameController gm;
    float t = 0;

    public SO_StatisticheShooting SO_stat;
    public SO_Input SO_input;

    private void Start()
    {
        tor = GameObject.Find("Torretta").GetComponent<Animator>();
        pla = GameObject.Find("PlayerOnCamper").GetComponent<Animator>();

    }

    void Update()
    {
        t += Time.deltaTime;

        //rotation
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f))
        {
            transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z));
            if (gameObject.tag == "Player")
            {
                transform.Rotate(Vector3.right, 90);
                transform.Rotate(Vector3.forward, 90);
            }
            else
                transform.Rotate(Vector3.up, 90);
            //print(hit.point.x + " " + transform.position.y + " " + hit.point.z);
        }
        //shoot
        if (Input.GetKey(SO_input.sparo) && (gameObject.tag == "Player" || gm.ammo > 0) && t > SO_stat.fireRate)
        {
            GameObject spawned;
            t = 0;
            //sul camper
            if (gameObject.tag == "Camper")
            {
                gm.ammo--;
                spawned = Instantiate(SO_stat.proiettile, transform.position + transform.right * -SO_stat.verticalOffsetShot + transform.forward * -SO_stat.horizOffsetShot, transform.rotation);
                spawned.transform.Rotate(Vector3.up, -90);
                spawned.transform.Rotate(Vector3.right, 90);
                spawned.transform.Rotate(Vector3.forward, 90);
                tor.SetBool("Shoot", true);
                pla.SetBool("it_shoot", true);
            }
            //a terra
            else if (gameObject.tag == "Player")
            {
                //creazione proiettile e scelta offset
                spawned = Instantiate(SO_stat.proiettile, transform.position + transform.right * SO_stat.verticalOffsetShot + transform.up * SO_stat.horizOffsetShot, transform.rotation);
                spawned.transform.Rotate(Vector3.forward, -3);
                GetComponent<Animator>().SetBool("Shooting", true);
            }
        }
        else
        {
            if (gm.mode == "ONGROUND")
            {
                GetComponent<Animator>().SetBool("Shooting", false);
            }
                tor.SetBool("Shoot", false);
                pla.SetBool("it_shoot", false);
        }
    }

}
