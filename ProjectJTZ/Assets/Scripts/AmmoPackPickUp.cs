﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPackPickUp : MonoBehaviour {

    public SO_GameController gm;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            gm.ammo += Random.Range(10,20);
            Destroy(gameObject);
        }
    }
}
