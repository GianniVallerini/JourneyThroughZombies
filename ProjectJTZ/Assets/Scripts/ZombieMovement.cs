﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieMovement : MonoBehaviour {

    public SO_StatisticheZombie SO_statZ;

    GameObject target;
    GameObject player;
    GameObject camper;
    int focusSwitch = 0;
    float distanceFromPlayer = 10f;
    float distanceFromCamper = 9f;

    NavMeshAgent navMeshAgent;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = SO_statZ.speed;
        camper = GameObject.FindGameObjectWithTag("Camper");
        target = camper;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update () {

        //Target Choice
        distanceFromCamper = Vector3.Distance(transform.position, camper.transform.position);
        distanceFromPlayer = Vector3.Distance(transform.position, player.transform.position);

        //cambia target solo se ha switch di focus disponibili
        if (focusSwitch <= SO_statZ.maxFocusSwitch)
        {
            if (distanceFromPlayer < distanceFromCamper && player.GetComponent<Renderer>().isVisible)
            {
                if (target == camper)
                    focusSwitch++;
                target = player;
            }
            else
            {
                if (target == player)
                {
                    focusSwitch++;
                    target = camper;
                }
                target = camper;
            }
        }
        else
            target = camper;

        transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
        transform.Rotate(Vector3.right, 90);
        transform.Rotate(Vector3.forward, 90);

        //NavMesh Destination
        SetDestination();

    }

    void SetDestination()
    {
        if(target != null)
        {
            navMeshAgent.SetDestination(target.transform.position);
        }
    }
}
