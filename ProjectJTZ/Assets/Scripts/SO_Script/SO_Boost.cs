﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bst_", menuName = "Boost")]
public class SO_Boost : ScriptableObject {

    public string boostName;
    public int woodReq;
    public int metalReq;
    public int erbsReq;
    public int medsReq;
    public Sprite icon;
    public SO_BoostEffect effect;
}
