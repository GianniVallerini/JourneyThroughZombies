﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ListaSprites", menuName = "ListaSprites")]
public class SO_ListaSprites : ScriptableObject {

    public List<Sprite> Lista;

}
