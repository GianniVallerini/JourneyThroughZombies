﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StatisticheZombie", menuName = "StatisticheZombie")]
public class SO_StatisticheZombie : ScriptableObject {

    public int hp;
    public float speed;
    public float damage;
    public float attackTimerSec;
    public float ammoDropPercent;
    public int maxFocusSwitch;
}
