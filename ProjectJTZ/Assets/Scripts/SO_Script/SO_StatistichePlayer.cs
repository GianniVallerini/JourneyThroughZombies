﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StatistichePlayer", menuName = "StatistichePlayer")]
public class SO_StatistichePlayer : ScriptableObject {

    public float speed;
    public float sprintSpeed;
    public float stamina;
    public float riduzioneStamina;
}
