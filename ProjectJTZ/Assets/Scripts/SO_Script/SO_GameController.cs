﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameManager", menuName = "GameManager")]
public class SO_GameController : ScriptableObject
{

    public string mode = "ONGROUND";
    public string playerMode = "COMBAT";

    public int ammo = 100;
    public int wood = 15;
    public int metal = 10;
    public int meds = 5;
    public int erbs = 15;

    public float playerHp = 100;
    public float camperHp = 100;

}
