﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Str_", menuName = "Struttura")]
public class SO_Struttura : ScriptableObject {

    public string buildingName;
    public int woodReq;
    public int metalReq;
    public int erbsReq;
    public Sprite icon;
    public Collider collider;
    public GameObject prefab;
    
    public float hp; //vita
    public float damageTimerInSec; //ogni quanto fa danno (filo spinato)
    public float damage; //danno
    public float triggerRadius; //area di attivazione trappola
    public float aoeRadius; //area di danno trappola o muro spinato

}
