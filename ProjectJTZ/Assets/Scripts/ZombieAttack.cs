﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieAttack : MonoBehaviour {

    public SO_StatisticheZombie SO_statZ;
    public SO_GameController gm;
    public GameEvent LoseEvent;

    float t = 0;
    NavMeshAgent navMeshAgent;
    Rigidbody rb;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        t -= Time.deltaTime;   
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Camper" || collision.collider.tag == "Player")
        {
            navMeshAgent.enabled = false;
            rb.isKinematic = false;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        
        if(collision.collider.tag == "Player" && t <= 0)
        {
            t = SO_statZ.attackTimerSec;
            gm.playerHp -= SO_statZ.damage;
            if (gm.playerHp <= 0)
                LoseEvent.Raise();
        }

        if (collision.collider.tag == "Camper" && t <= 0)
        {
            t = SO_statZ.attackTimerSec;
            gm.camperHp -= SO_statZ.damage;
            if (gm.camperHp <= 0)
                LoseEvent.Raise();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Camper" || collision.collider.tag == "Player")
        {
            navMeshAgent.enabled = true;
            rb.isKinematic = true;
        }
    }
}
