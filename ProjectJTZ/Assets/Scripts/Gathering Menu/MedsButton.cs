﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedsButton : MonoBehaviour {

    public Button myButton;
    public int minGather = 0;
    public int maxGather = 10;
    public SO_GameController gm;

    private void Start()
    {
        myButton.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        gm.meds += Random.Range(minGather, maxGather+1);
    }
}
