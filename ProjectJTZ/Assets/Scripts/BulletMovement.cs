﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour {

    public float speed = 0.5f;
    public float bulletLife = 4f;
    public int damage;
    float t = 0f;

	void Update () {

        t += Time.deltaTime;
        if (t > bulletLife) Destroy(gameObject);

        transform.position += transform.right * speed;

	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enviroment")
        {
            Destroy(gameObject);
        }

        if (other.tag == "Zombie")
        {
            other.GetComponent<ZombieStats>().hp -= damage;
            Destroy(gameObject);
        }
    }
}
