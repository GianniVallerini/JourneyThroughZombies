﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour {

    public GameObject zombie;
    public float spawnTime = 1f;
    public int spawnNumber = 1;
    public int spawnTimes = 10;

    float t;

    private void Start()
    {
        t = Random.Range(30f, 30f + spawnTime);
    }

    void Update () {

        t -= Time.deltaTime;

        if(t <= 0 && spawnTimes > 0)
        {
            SpawnAll();
            t = spawnTime;
            spawnTimes--;
        }

	}

    public void SpawnAll()
    {
        for (int i = 0; i < spawnNumber; i++)
        {
            Spawn();
        }
    }

    public void Spawn()
    {
        GameObject spawned;
        spawned = Instantiate(zombie, transform.position, transform.rotation);
    }
}
