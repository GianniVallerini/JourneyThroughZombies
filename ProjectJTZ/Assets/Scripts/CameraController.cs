﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public GameObject camper;
    Vector3 cursorPosition;
    Vector3 targetPos;
    Vector3 normalizedMidV;
    Vector3 midVector;
    public SO_GameController gm;
    public float cameraDistanceFromPlayer = 0.2f;
    public float cameraSpeed = 0.1f;

	void Start () {
        cursorPosition = Vector3.zero;
        targetPos = Vector3.zero;
	}

	void FixedUpdate () {

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f))
        {
            cursorPosition = new Vector3(hit.point.x, 0, hit.point.z);
        }
        if (gm.mode == "ONGROUND")
        {
            midVector = cursorPosition - player.transform.position;
            normalizedMidV = midVector.normalized;
            targetPos = cameraDistanceFromPlayer * midVector + player.transform.position;
        }
        else
        {
            midVector = cursorPosition - camper.transform.position;
            normalizedMidV = midVector.normalized;
            targetPos = cameraDistanceFromPlayer * midVector + camper.transform.position;
        }
        
        transform.position += Vector3.right * (Difference(targetPos.x, transform.position.x) * cameraSpeed);
        transform.position += Vector3.forward * (Difference(targetPos.z, transform.position.z) * cameraSpeed);
    }

    float Difference(float a, float b)
    {
        return a - b;
    }
}
